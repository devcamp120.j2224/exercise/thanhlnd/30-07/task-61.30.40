package com.devcamp.task60s70.customerorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60s70.customerorder.models.CProduct;

public interface IProductRepository extends JpaRepository <CProduct, Long> {
    
}

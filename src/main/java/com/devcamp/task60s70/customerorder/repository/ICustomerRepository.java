package com.devcamp.task60s70.customerorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60s70.customerorder.models.CCustomer;

public interface ICustomerRepository extends JpaRepository <CCustomer, Long>{
    CCustomer findById(long id);
}

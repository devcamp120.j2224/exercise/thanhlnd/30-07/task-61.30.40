package com.devcamp.task60s70.customerorder.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task60s70.customerorder.models.CCustomer;
import com.devcamp.task60s70.customerorder.models.COrder;
import com.devcamp.task60s70.customerorder.repository.ICustomerRepository;

@RestController
@CrossOrigin
@RequestMapping
public class COrderController {
    @Autowired
    ICustomerRepository customerRepository;
    @GetMapping("devcamp-orders")
    public ResponseEntity <Set<COrder>> getCarTypeByCarCode(@RequestParam(value = "userId")long userId){
        try {
            CCustomer vCustomer = customerRepository.findById(userId);
            if (vCustomer != null) {
                return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
